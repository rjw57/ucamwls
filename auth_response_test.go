package ucamwls

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"
)

func TestGoodRequest(t *testing.T) {
	req := newGoodRequest(t)

	_, err := NewAuthResponse(req)
	if err != nil {
		t.Fatal(err)
	}
}

func TestSignResponse(t *testing.T) {
	req := newGoodRequest(t)

	resp, err := NewAuthResponse(req)
	if err != nil {
		t.Fatal(err)
	}

	resp.SetSuccess("spqr1", AuthPassword)

	key := newKey(t)
	r, err := resp.SignedResponseData(rand.Reader, 1, key)
	if err != nil {
		t.Fatal(err)
	}

	if r == "" {
		t.Fatal("No signature generated")
	}
}

func TestEscapeResponseComponent(t *testing.T) {
	check := func(in string, expected string) {
		enc := escapeResponseComponent(in)
		if enc != expected {
			t.Logf("From '%s' expected '%s' but got '%s'", in, expected, enc)
			t.Fail()
		}
	}

	check("no replace", "no replace")
	check("%!%!!%", "%25%21%25%21%21%25")
	check("one!", "one%21")
}

func newGoodRequest(t *testing.T) *AuthRequest {
	r := newGetRequest(t, newGoodValues())

	req, err := NewAuthRequest(r)
	if err != nil {
		t.Fatal(err)
	}

	return req
}

func newKey(t *testing.T) *rsa.PrivateKey {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Fatal(err)
	}
	return key
}
