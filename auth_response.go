package ucamwls

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
)

const (
	// StatusUnset indicates the status has not yet been set
	StatusUnset = ""

	// StatusSuccess indicates authentication succeeded.
	StatusSuccess = "200"

	// StatusCancelled indicates that the user cancelled the authentication request.
	StatusCancelled = "410"

	// StatusNoAcceptableAuthTypes indicates that the WLS and WAA have no acceptable
	// authentication types in common.
	StatusNoAcceptableAuthTypes = "510"

	// StatusUnsupportedProtocol indicates that the WAA is using a protocol version
	// which is not supported by this WLS.
	StatusUnsupportedProtocol = "520"

	// StatusGeneralRequestError indicates a generic problem decoding the authentication
	// request.
	StatusGeneralRequestError = "530"

	// StatusInteractionRequired indicates that user interaction would be required
	// to authenticate the user but the WAA indicated that the authentication should be
	// non-interactive.
	StatusInteractionRequired = "540"

	// StatusNotAuthorised indicates that the WAA is not authorised to use this WLS.
	StatusNotAuthorised = "560"

	// StatusDeclined indicates that this WLS declined to provide authentication services
	// to the WAA.
	StatusDeclined = "570"

	// AuthPassword is the password authentication auth type.
	AuthPassword = "pwd"
)

// AuthResponse represents a response to a WAA from a WLS. An encoded and signed form of the
// response is sent back to the WAA via the WLS-Response query parameter.
type AuthResponse struct {
	// A unique identifier for the response. WAAs view the combination of
	// Identifier and IssueTime as unique.
	Identifier string

	// The time at which this response was constructed.
	IssueTime time.Time

	// Fields which are copied verbatim from the original request. Version
	// is never higher than the one specified in the request.
	Version string
	URL     *url.URL
	Params  string

	// If authentication was successful, this is the type of authentication
	// which succeeded.
	AuthType string

	// If AuthType is empty, authentication must have been established based on
	// previous successful authentication interaction(s) with the user. This
	// indicates which authentication types were used on these occasions.
	SSO []string

	// Status if the authentication request. Usually this is one of the Status...
	// constants.
	Status string

	// An optional message from the WLS describing the reasons for the status of the
	// response.
	Message string

	// The authenticated identity of the user. Required if Status is StatusSuccess.
	Principal string

	// For version 3 responses this indicates attributes or properties of the principal.
	// Values of these tags are not standardised. For the Raven service at the University
	// of Cambridge, the value "current" is used to indicate a current member of staff
	// or student.
	PTags []string

	// If the user has established a session with the WLS, this indicates the
	// remaining lifetime of the session. The zero-value indicates the session lifetime
	// is open-ended.
	Lifetime time.Duration
}

// NewAuthResponse constructs an AuthResponse from a request.
//
// The issue time is set to time.Now() but may be overridden later if the
// response is not to be given immediately.
//
// A random identifier is generated for convenience but may be modified.
func NewAuthResponse(req *AuthRequest) (*AuthResponse, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	r := &AuthResponse{
		Identifier: id.String(),
		Version:    req.Version,
		URL:        req.URL,
		Params:     req.Params,
		IssueTime:  time.Now(),
	}

	return r, nil
}

// ResponseData converts the response into a parameter string suitable for signing.
func (r *AuthResponse) ResponseData() string {
	values := []string{
		r.Version,
		r.Status,
		r.Message,
		r.IssueTime.UTC().Format("20060102T150405Z"),
		r.Identifier,
		r.URL.String(),
	}

	if r.Status == StatusSuccess {
		values = append(values, r.Principal)
	} else {
		values = append(values, "")
	}

	if r.Version == "3" {
		values = append(values, strings.Join(r.PTags, ","))
	}

	values = append(
		values,
		r.AuthType,
		strings.Join(r.SSO, ","),
		strconv.FormatInt(int64(r.Lifetime.Seconds()), 10),
		r.Params,
	)

	data := []string{}
	for _, v := range values {
		data = append(data, escapeResponseComponent(v))
	}

	return strings.Join(data, "!")
}

// SignedResponseData takes a numeric keyid and private key, constructs a
// Base64-encoded signature and returns a signed response string suitable for
// passing back to the WAA via the WLS-Response parameter.
func (r *AuthResponse) SignedResponseData(rand io.Reader, kid int64, priv *rsa.PrivateKey) (string, error) {
	if kid <= 0 {
		return "", fmt.Errorf("Key id '%v' must be greater than zero", kid)
	}

	if r.Status == StatusUnset {
		return "", fmt.Errorf("Status not yet set")
	}

	rdata := r.ResponseData()

	// The response data must first be hash-ed using the SHA-1 hash
	hashed := sha1.Sum([]byte(rdata))

	// Sign the resulting hash
	sig, err := rsa.SignPKCS1v15(rand, priv, crypto.SHA1, hashed[:])
	if err != nil {
		return "", err
	}

	s := fmt.Sprintf("%s!%s!%s", rdata,
		escapeResponseComponent(strconv.FormatInt(kid, 10)),
		escapeResponseComponent(encodeWebAuthBase64(sig)),
	)

	return s, err
}

// SignedResponseURL takes the same parameters as SignedResponseData but returns the
// URL to redirect to.
func (r *AuthResponse) SignedResponseURL(rand io.Reader, kid int64, priv *rsa.PrivateKey) (*url.URL, error) {
	data, err := r.SignedResponseData(rand, kid, priv)
	if err != nil {
		return nil, err
	}

	u, err := url.Parse(r.URL.String())
	if err != nil {
		// Should not happen
		panic(err)
	}

	// For version 1 requests, the existing query string is ignored
	if r.Version == "1" {
		u.RawQuery = ""
	}

	vs := u.Query()
	vs.Add("WLS-Response", data)
	u.RawQuery = vs.Encode()

	return u, nil
}

// SetSuccess sets the response as successful for the passed principal. For compatibility
// with the Raven implementation, this will also add the "current" PTag for v3 responses.
func (r *AuthResponse) SetSuccess(principal string, authType string) {
	r.Status = StatusSuccess
	r.Principal = principal
	r.AuthType = authType

	if r.Version == "3" {
		r.PTags = append(r.PTags, "current")
	}
}

// escapeResponseComponent escapes "!" and "%" characters as described in the WebAuth spec
// by replacing them with "%21" and "%25" respectively.
func escapeResponseComponent(s string) string {
	r := strings.NewReplacer("!", "%21", "%", "%25")
	return r.Replace(s)
}

// encodeWebAuthBase64 encodes the passed data to the variant of Base64 described in the WebAuth
// spec. This is similar in spirit to URL-safe Base64 but different in defails. In this
// encoding, the characters '+', '/', and '=' are replaced by '-', '.' and '_' .
func encodeWebAuthBase64(data []byte) string {
	return strings.Map(func(r rune) rune {
		switch r {
		case '+':
			return '-'
		case '/':
			return '.'
		case '=':
			return '_'
		}
		return r
	}, base64.StdEncoding.EncodeToString(data))
}
