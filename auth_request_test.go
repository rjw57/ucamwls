package ucamwls

import (
	"net/http"
	"net/url"
	"strings"
	"testing"
)

func TestGoodValues(t *testing.T) {
	r := newGetRequest(t, newGoodValues())

	_, err := NewAuthRequest(r)
	if err != nil {
		t.Fatal(err)
	}
}

func TestMissingRequiredParameters(t *testing.T) {
	for _, k := range []string{"ver", "url"} {
		t.Logf("Testing removal of '%s'", k)
		vs := newGoodValues()
		vs.Del(k)

		r := newGetRequest(t, vs)
		err := checkNewAuthRequestFails(t, r)
		t.Log("Error:", err)
		if !strings.Contains(err.Error(), k) {
			t.Fatal("Error did not contain parameter name")
		}
	}
}

func TestUnknownAdditionalParameter(t *testing.T) {
	param := "unknown"
	vs := newGoodValues()
	vs.Set(param, "some-value")

	r := newGetRequest(t, vs)
	err := checkNewAuthRequestFails(t, r)
	t.Log("Error:", err)
	if !strings.Contains(err.Error(), param) {
		t.Fatal("Error did not contain parameter name")
	}
}

func TestMultipleValues(t *testing.T) {
	for k := range *newGoodValues() {
		vs := newGoodValues()
		vs.Add(k, vs.Get(k))

		r := newGetRequest(t, vs)
		err := checkNewAuthRequestFails(t, r)
		t.Log("Error:", err)
		if !strings.Contains(err.Error(), k) {
			t.Fatal("Error did not contain parameter name")
		}
	}
}

func TestASCIIParameters(t *testing.T) {
	for _, k := range []string{"desc", "msg"} {
		vs := newGoodValues()
		vs.Set(k, "with\nnewline")
		t.Log("Setting", k, "to", vs.Get(k))

		r := newGetRequest(t, vs)
		err := checkNewAuthRequestFails(t, r)
		t.Log("Error:", err)
		if !strings.Contains(err.Error(), k) {
			t.Fatal("Error did not contain parameter name")
		}

		vs.Set(k, "with non-ASCII ø→↓←ŧeþþ")
		t.Log("Setting", k, "to", vs.Get(k))

		r = newGetRequest(t, vs)
		err = checkNewAuthRequestFails(t, r)
		t.Log("Error:", err)
		if !strings.Contains(err.Error(), k) {
			t.Fatal("Error did not contain parameter name")
		}
	}
}

func TestBadURL(t *testing.T) {
	vs := newGoodValues()
	vs.Set("url", ":///////NOT-A-URL")

	r := newGetRequest(t, vs)
	checkNewAuthRequestFails(t, r)
}

func TestBadURLScheme(t *testing.T) {
	vs := newGoodValues()
	vs.Set("url", "ftp://waa.invalid/foo")

	r := newGetRequest(t, vs)
	err := checkNewAuthRequestFails(t, r)
	t.Log("Error:", err)
	if !strings.Contains(err.Error(), "ftp") {
		t.Fatal("Error did not contain URL scheme")
	}
}

func TestBadQuery(t *testing.T) {
	vs := newGoodValues()

	r := newGetRequest(t, vs)
	r.URL.RawQuery = "%2"

	// Check that the query string is invalid
	if _, err := url.ParseQuery(r.URL.RawQuery); err == nil {
		t.Fatal("Did not get expected error")
	}

	checkNewAuthRequestFails(t, r)
}

func newGetRequest(t *testing.T, vs *url.Values) *http.Request {
	u, err := url.Parse("http://example.invalid/base")
	if err != nil {
		t.Fatal(err)
	}
	u.RawQuery = vs.Encode()

	t.Logf("Requesting %s", u.String())
	r, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		t.Fatal(err)
	}

	return r
}

func TestBadVersion(t *testing.T) {
	vs := newGoodValues()
	vs.Set("ver", "bas")

	r := newGetRequest(t, vs)
	checkNewAuthRequestFails(t, r)
}

func TestGoodVersions(t *testing.T) {

	for _, ver := range []string{"1", "2", "3"} {
		vs := newGoodValues()
		vs.Set("ver", ver)
		r := newGetRequest(t, vs)

		_, err := NewAuthRequest(r)
		if err != nil {
			t.Fatal(err)
		}
	}
}

func newGoodValues() *url.Values {
	return &url.Values{
		"ver":    {"3"},
		"url":    {"http://waa.invalid/foo/bar"},
		"desc":   {"Test description"},
		"msg":    {"Test message"},
		"aauth":  {"pwd"},
		"iact":   {"yes"},
		"params": {"Some parameters which must not be parsed"},
		"date":   {"IGNORED"},
		"skew":   {"IGNORED"},
		"fail":   {"yes"},
	}
}

func checkNewAuthRequestFails(t *testing.T, r *http.Request) error {
	ar, err := NewAuthRequest(r)
	t.Log("Return value:", ar)
	t.Log("Error:", err)
	if err == nil {
		t.Fatal("Did not get expected error")
	}

	if ar != nil {
		t.Fatal("Returned non-nil value from function expected to fail")
	}

	return err
}
