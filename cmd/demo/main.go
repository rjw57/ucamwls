package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"log"
	"net/http"

	ucamwls "../.."
)

const keyPEMData = `
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAwfwX5j4Iyde64KHhK8RtGfAidqy2kqvPdCOlphaRAiOCTGI9
5lj8aaWPcl1QTj6gp1eSg19vj/cUpeasRxSgiFAoZU59MM1WqNSnhkiMiZUqUthv
qeuRgeI5grDzTAww9g9gv3DeLfhaEnoyZa7agYVo9iLT5w878bWmOTY4U815P/S4
8cmLI9ABayU6GC4TzhLI+TN55F6/tZ22fKYwTQsE9CSwT1DE6uFD1StXJ6rCkkEg
dSscpV8pimMYd9zT1y+QpE3jEsiLYaIbXS4HvxEB9qb+488U+96DFmnVEOzLbeT7
Iy4ABeDqsU2KcwYToL82LbFZiUlGJZQSOUt0tQIDAQABAoIBACbt0F4EVP8sq/Qo
iGegI8Twy/nE716/3AISpUuPe8pSFfP+Qy3W7dDFyOPKljMdnl/20xBKNrlNfHdO
s4QQP6cbbv/loHoKuzlmc7gfFxUFOlkl1kLjY3ANVj1EBY9dYvX+YAwLOos/K9RJ
cZO2Y2NawpWip6PXCtCXA6S3nNY0LIrn/321bFy/F/rlIeFLuoyiC+Y5hbX0DZIE
cs/sbB/ctIcc9i/FA5Wu/Em2FtrWm/pYwszyGw1vxf0gUO7h2aj/dTcoFn6GUbgN
mM9CosRKpCvnHzAFrl/M2ZXpRHMW7g0C8yUgLbczxihktRRWVArzF3efQZZT8yjp
LrChNIECgYEA42+XNjB75D80RA9PpI4/+a7agGV4iPU2J09Q9eW9uNdiGQszS/Hi
7DzjPrllPjivpoYm5RZsXt0yfhhuuhGdM5O3FU+6oC8RK+cRma5z1wnAqTgkaTuT
3lCUuCEEwjFeqEZ6Ok6w+mSM0EN//A0ZTtUUEd73yfb6XnOEN2ZS/W8CgYEA2lj/
QT4X45SoaqJGYvYLdTW1oD2rIxXl4ShI68s+f/DRAuHAPOBTTyDcqHFfUAogCadq
o0dKu0yioxjC42XiB3LxWoA6jEJ0aNp/+9OQutnNYQaLFsOK+OXuXAil9krHa1SO
QPv52jJtsT0B7TW94+oM4rKQlWFOGPamIVQ35hsCgYEAjQKt9QogdkZ9qEj57yPH
JI41qO/AyOGHvt2da6gSdh0MFEzudAK32BJ0LnqR+/73YL7CNL0vWa25VZbeZ/zt
w4GtPMhfBFvKQLcWIXuZFNVWH1TQtnIa3vaFDphiM+iaMKjptWb+sXBQIfwHyQVy
EH9p3x6n8Q9w8cibTCHkCH8CgYEAjxarkFHkzdtp0xetEMU1FQaxk+VV9ZQwJbGE
uCFlP+rMzhqrJDnN3pkmgaxqTcWU4T65f28E5mF+Y2uiq+rkiQUmbayCsdALAXvP
S3SF8C0zjlX/aFeVBy+HWQDB4fl3Xjr3TlbSkc9iWzgjvxPs8SM8doOWhI5T48Af
ds/+IekCgYBFUfxLQBznnY+8X+FvmHLOpS+96Q+j+QTbiOhhbbYY7ge2xpzcYgX8
gXkVxIMHR8OjzTcbyXKavFUq3C9grCSTp1IqI8SLSkqcOCYx82fBA3148+e8EbFK
/8kTtsqYBWENcsI/63WmP8cAT2yl+baoA0sQYNBGLO9mxpn4CBDdJQ==
-----END RSA PRIVATE KEY-----
`

var key *rsa.PrivateKey

func authHandler(w http.ResponseWriter, r *http.Request) {
	ar, err := ucamwls.NewAuthRequest(r)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Bad request: %v", err)
		return
	}

	resp, err := ucamwls.NewAuthResponse(ar)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Bad request: %v", err)
		return
	}

	resp.SetSuccess("spqr1", ucamwls.AuthPassword)

	url, err := resp.SignedResponseURL(rand.Reader, 1, key)
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, "Internal error: %v", err)
		return
	}

	w.Header().Add("Location", url.String())
	w.WriteHeader(302)
}

func loadPrivateKey(s string) (*rsa.PrivateKey, error) {
	data := []byte(s)

	for block, bs := pem.Decode(data); bs != nil; block, bs = pem.Decode(data) {
		if block.Type != "RSA PRIVATE KEY" {
			continue
		}

		priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return nil, err
		}

		return priv, nil
	}

	return nil, fmt.Errorf("No private key block found")
}

func main() {
	var err error

	key, err = loadPrivateKey(keyPEMData)
	if err != nil {
		panic(fmt.Sprintf("Error loading private key: %v", err))
	}

	http.HandleFunc("/auth/authenticate.html", authHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
